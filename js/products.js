(function(){
	var app = angular.module( 'store-products', [] );

	// Product title directive
	app.directive('productTitle', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product-title.html'
		};
	});

	// Product tabs/panels directive
	app.directive('productPanels', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product-panels.html',
			controller: function(){
				this.tab = 1;

				this.selectTab = function(setTab) {
					this.tab = setTab;
				};

				this.isSelected = function(checkTab){
					return this.tab === checkTab;
				};

			},
			controllerAs: 'panel'
		};
	});

	// Product reviews directive
	app.directive('productReviews', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product-reviews.html'
		};
	});

	// Submit review form directive
	app.directive('productReviewForm', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product-review-form.html',
			controller: function(){
				this.review = {};

				this.addReview = function(product) {
					product.reviews.push(this.review);
					this.review = {};
				};
			},
			controllerAs: 'reviewFormCtrl'
		};
	});

})();